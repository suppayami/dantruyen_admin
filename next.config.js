const path = require('path')

module.exports = {
    publicRuntimeConfig: {
        siteName: 'DTAdmin',
    },
    experimental: {
        async rewrites() {
            return [
                {
                    source: '/graphql',
                    destination: 'http://localhost:4000/graphql',
                },
                {
                    source: '/graphql/upload',
                    destination: 'http://localhost:4000/graphql/upload',
                },
            ]
        },
    },
    webpack(config, options) {
        config.resolve.alias['@'] = path.join(__dirname, 'src')
        return config
    },
}
