import { gql } from '@apollo/client'

export const AuthenticateMutation = gql`
    mutation Authenticate($username: String!, $password: String!) {
        authenticate(username: $username, password: $password) {
            user {
                id
                username
                displayName
                email
                userGroup {
                    id
                    name
                    isAdmin
                }
            }
            sessionToken
        }
    }
`
