/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: RenewSession
// ====================================================

export interface RenewSession_renewSession_user_userGroup {
  __typename: "UserGroup";
  id: any;
  name: string;
  isAdmin: boolean;
}

export interface RenewSession_renewSession_user {
  __typename: "User";
  id: any;
  username: string;
  displayName: string;
  email: string;
  userGroup: RenewSession_renewSession_user_userGroup;
}

export interface RenewSession_renewSession {
  __typename: "AuthenticateResult";
  sessionToken: string;
  user: RenewSession_renewSession_user;
}

export interface RenewSession {
  renewSession: RenewSession_renewSession | null;
}
