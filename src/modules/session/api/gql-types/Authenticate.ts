/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: Authenticate
// ====================================================

export interface Authenticate_authenticate_user_userGroup {
  __typename: "UserGroup";
  id: any;
  name: string;
  isAdmin: boolean;
}

export interface Authenticate_authenticate_user {
  __typename: "User";
  id: any;
  username: string;
  displayName: string;
  email: string;
  userGroup: Authenticate_authenticate_user_userGroup;
}

export interface Authenticate_authenticate {
  __typename: "AuthenticateResult";
  user: Authenticate_authenticate_user;
  sessionToken: string;
}

export interface Authenticate {
  authenticate: Authenticate_authenticate | null;
}

export interface AuthenticateVariables {
  username: string;
  password: string;
}
