import { gql } from '@apollo/client'

export const RenewSessionMutation = gql`
    mutation RenewSession {
        renewSession {
            sessionToken
            user {
                id
                username
                displayName
                email
                userGroup {
                    id
                    name
                    isAdmin
                }
            }
        }
    }
`
