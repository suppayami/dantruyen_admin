import * as React from 'react'
import { useRouter } from 'next/router'
import { observer } from 'mobx-react-lite'

import { useSessionStore } from '@/modules/session/hook/session_store.hook'
import { useRenewSession } from '@/modules/session/hook/renew_session.hook'
import { useAutorun } from '@/common/hooks/useAutorun'
import { isGuest, isAuthenticated } from '@/modules/session/store/authenticate.state'
import { isRemember } from '@/common/service/auth.link'

interface ProtectedProps {
    isProtected?: boolean
}

export const Protected: React.FunctionComponent<ProtectedProps> = observer(function Protected({
    isProtected,
    children,
}) {
    const sessionStore = useSessionStore()
    const router = useRouter()

    useRenewSession()
    useAutorun(() => {
        if (!isProtected) {
            return
        }
        if (!isRemember() || isGuest(sessionStore.authenticateState)) {
            router.replace('/login')
        }
    })

    return (
        <React.Fragment>
            {isProtected && !isAuthenticated(sessionStore.authenticateState)
                ? 'Loading...'
                : children}
        </React.Fragment>
    )
})
