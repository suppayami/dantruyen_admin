import { useEffect } from 'react'

import { useSessionStore } from '@/modules/session/hook/session_store.hook'
import { isRemember } from '@/common/service/auth.link'

export const useRenewSession = () => {
    const sessionStore = useSessionStore()

    useEffect(() => {
        if (isRemember()) {
            sessionStore.renewSession()
        }
    }, [])
}
