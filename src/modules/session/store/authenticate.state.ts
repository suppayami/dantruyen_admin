type GuestState = {
    state: 'guest'
}

type AuthenticatingState = {
    state: 'authenticating'
}

type AuthenticatedState = {
    state: 'authenticated'
    token: string
    profile: ProfileState
}

export type ProfileState = {
    id: string
    username: string
    displayName: string
    email: string
    userGroup: {
        id: string
        name: string
        isAdmin: boolean
    }
}

export type AuthenticateState = GuestState | AuthenticatingState | AuthenticatedState

export type AuthenticatedPayload = { profile: ProfileState; token: string }

/**
 * Guest State constructor
 */
export const guestState = (): AuthenticateState => ({
    state: 'guest',
})

/**
 * Authenticating State constructor
 */
export const authenticatingState = (): AuthenticateState => ({
    state: 'authenticating',
})

/**
 * Authenticated State constructor
 */
export const authenticatedState = (payload: AuthenticatedPayload): AuthenticateState => ({
    state: 'authenticated',
    ...payload,
})

export const isGuest = (state: AuthenticateState): state is GuestState => state.state === 'guest'
export const isAuthenticated = (state: AuthenticateState): state is AuthenticatedState =>
    state.state === 'authenticated'
export const isAuthenticating = (state: AuthenticateState): state is AuthenticatingState =>
    state.state === 'authenticating'
