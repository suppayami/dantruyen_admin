import { observable, action, flow } from 'mobx'
import { FetchResult } from '@apollo/client'

import { isBrowser } from '@/ssr/is_platform.ssr'
import { setToken, setRemember } from '@/common/service/auth.link'
import { RenewSession } from '@/modules/session/api/gql-types/RenewSession'
import { RenewSessionMutation } from '@/modules/session/api/renew_session'
import { notifyError } from '@/common/notification/toast'
import { i18n } from '@/common/i18n/i18n'
import { AuthenticateVariables, Authenticate } from '@/modules/session/api/gql-types/Authenticate'
import { AuthenticateMutation } from '@/modules/session/api/authenticate'
import { getApolloClient } from '@/common/service/apollo'
import { LogoutMutation } from '@/modules/session/api/logout'

import {
    AuthenticateState,
    guestState,
    authenticatedState,
    AuthenticatedPayload,
    authenticatingState,
    isAuthenticating,
} from './authenticate.state'

export type SessionState = {
    authenticateState: AuthenticateState
}

export const makeSessionStore = (initialState?: SessionState) => {
    const store = observable(
        {
            authenticateState: guestState(),
            ...initialState,

            renewSession: flow(function* renewSession(silent?: boolean) {
                if (isAuthenticating(store.authenticateState)) {
                    return
                }
                if (!silent) {
                    store.authenticating()
                }
                try {
                    const apolloClient = getApolloClient({})
                    const { data }: FetchResult<RenewSession> = yield apolloClient.mutate({
                        mutation: RenewSessionMutation,
                    })
                    if (data?.renewSession) {
                        store.setSession({
                            token: data?.renewSession?.sessionToken,
                            profile: data?.renewSession?.user,
                        })
                    }
                } catch (err) {
                    store.logout()
                }
            }),

            authenticate: flow(function* authenticate(payload: AuthenticateVariables) {
                if (isAuthenticating(store.authenticateState)) {
                    return
                }
                store.authenticating()
                try {
                    const apolloClient = getApolloClient({})
                    const { data }: FetchResult<Authenticate> = yield apolloClient.mutate({
                        mutation: AuthenticateMutation,
                        variables: payload,
                    })
                    if (data?.authenticate) {
                        store.setSession({
                            token: data?.authenticate?.sessionToken,
                            profile: data?.authenticate?.user,
                        })
                    }
                } catch (err) {
                    store.logout()
                    notifyError(err.message)
                }
            }),

            logout: flow(function* logout() {
                const apolloClient = getApolloClient({})
                try {
                    yield apolloClient.mutate({ mutation: LogoutMutation })
                } catch {}
                store.authenticateState = guestState()
                if (isBrowser()) {
                    setToken('')
                    setRemember(false)
                }
            }),

            setSession(payload: AuthenticatedPayload) {
                if (!payload.profile.userGroup.isAdmin) {
                    this.logout()
                    notifyError(i18n.t('no_permission', { ns: 'notification' }))
                    return
                }
                this.authenticateState = authenticatedState(payload)
                if (isBrowser()) {
                    setToken(payload.token)
                    setRemember(true)
                }
            },

            authenticating() {
                if (isAuthenticating(this.authenticateState)) {
                    return
                }
                this.authenticateState = authenticatingState()
            },
        },
        {
            setSession: action,
            authenticating: action,
        },
    )

    return store
}

export type SessionStore = ReturnType<typeof makeSessionStore>
