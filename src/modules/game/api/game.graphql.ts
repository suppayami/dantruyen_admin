import { gql } from '@apollo/client'

export const GameQuery = gql`
    query Game($gameId: SID!) {
        game(id: $gameId) {
            id
            name
            popularScore {
                gameId
                score
            }
            premiumState
            description
            coverImageUrl
            thumbnailUrl
            gameDetail {
                id
                actLengthFirst
                actLengthSecond
                actLengthThird
                additionalRules
                adult
                ccLicense
                characterCreation
                contact
                gameMode
                gameSpeed
                licenseMode
                playerLimit
                storyGuideMode
                storyLength
                visibility
                wordLimitMode
                wordLimitNarrator
                wordLimitPlayer
            }
        }
    }
`
