import { gql } from '@apollo/client'

export const MakePremiumMutation = gql`
    mutation MakePremium($gameId: SID!) {
        adminMakePremium(gameId: $gameId) {
            id
        }
    }
`
