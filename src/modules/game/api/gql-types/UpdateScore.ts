/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateScore
// ====================================================

export interface UpdateScore_adminUpdateScore {
  __typename: "PopularScore";
  gameId: any;
  score: number;
}

export interface UpdateScore {
  adminUpdateScore: UpdateScore_adminUpdateScore;
}

export interface UpdateScoreVariables {
  gameId: any;
  score: number;
}
