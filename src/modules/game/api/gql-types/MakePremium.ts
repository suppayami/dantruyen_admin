/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: MakePremium
// ====================================================

export interface MakePremium_adminMakePremium {
  __typename: "Game";
  id: any;
}

export interface MakePremium {
  adminMakePremium: MakePremium_adminMakePremium;
}

export interface MakePremiumVariables {
  gameId: any;
}
