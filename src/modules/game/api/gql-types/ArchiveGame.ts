/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: ArchiveGame
// ====================================================

export interface ArchiveGame_archiveGame {
  __typename: "Game";
  id: any;
}

export interface ArchiveGame {
  /**
   * Archive game, if drafting, delete permanently.
   */
  archiveGame: ArchiveGame_archiveGame;
}

export interface ArchiveGameVariables {
  gameId: any;
}
