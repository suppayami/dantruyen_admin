/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GameUpdateInput, PremiumState, AdultType, GameMode } from "./../../../../common/global.type";

// ====================================================
// GraphQL mutation operation: UpdateGame
// ====================================================

export interface UpdateGame_updateGame_popularScore {
  __typename: "PopularScore";
  gameId: any;
  score: number;
}

export interface UpdateGame_updateGame_gameDetail {
  __typename: "GameDetail";
  id: any;
  actLengthFirst: number;
  actLengthSecond: number;
  actLengthThird: number;
  additionalRules: string;
  adult: AdultType;
  ccLicense: string;
  characterCreation: string;
  contact: string;
  gameMode: GameMode;
  gameSpeed: string;
  licenseMode: string;
  playerLimit: number;
  storyGuideMode: string;
  storyLength: string;
  visibility: string;
  wordLimitMode: string;
  wordLimitNarrator: number;
  wordLimitPlayer: number;
}

export interface UpdateGame_updateGame {
  __typename: "Game";
  id: any;
  name: string;
  popularScore: UpdateGame_updateGame_popularScore | null;
  premiumState: PremiumState;
  description: string | null;
  gameDetail: UpdateGame_updateGame_gameDetail;
}

export interface UpdateGame {
  updateGame: UpdateGame_updateGame;
}

export interface UpdateGameVariables {
  gameId: any;
  gameUpdateInput: GameUpdateInput;
}
