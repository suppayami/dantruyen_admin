/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PremiumState, AdultType, GameMode } from "./../../../../common/global.type";

// ====================================================
// GraphQL query operation: Game
// ====================================================

export interface Game_game_popularScore {
  __typename: "PopularScore";
  gameId: any;
  score: number;
}

export interface Game_game_gameDetail {
  __typename: "GameDetail";
  id: any;
  actLengthFirst: number;
  actLengthSecond: number;
  actLengthThird: number;
  additionalRules: string;
  adult: AdultType;
  ccLicense: string;
  characterCreation: string;
  contact: string;
  gameMode: GameMode;
  gameSpeed: string;
  licenseMode: string;
  playerLimit: number;
  storyGuideMode: string;
  storyLength: string;
  visibility: string;
  wordLimitMode: string;
  wordLimitNarrator: number;
  wordLimitPlayer: number;
}

export interface Game_game {
  __typename: "Game";
  id: any;
  name: string;
  popularScore: Game_game_popularScore | null;
  premiumState: PremiumState;
  description: string | null;
  coverImageUrl: string | null;
  thumbnailUrl: string | null;
  gameDetail: Game_game_gameDetail;
}

export interface Game {
  game: Game_game | null;
}

export interface GameVariables {
  gameId: any;
}
