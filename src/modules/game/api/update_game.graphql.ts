import { gql } from '@apollo/client'

export const UpdateGameMutation = gql`
    mutation UpdateGame($gameId: SID!, $gameUpdateInput: GameUpdateInput!) {
        updateGame(gameId: $gameId, gameUpdateInput: $gameUpdateInput) {
            id
            name
            popularScore {
                gameId
                score
            }
            premiumState
            description
            gameDetail {
                id
                actLengthFirst
                actLengthSecond
                actLengthThird
                additionalRules
                adult
                ccLicense
                characterCreation
                contact
                gameMode
                gameSpeed
                licenseMode
                playerLimit
                storyGuideMode
                storyLength
                visibility
                wordLimitMode
                wordLimitNarrator
                wordLimitPlayer
            }
        }
    }
`
