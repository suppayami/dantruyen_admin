import { gql } from '@apollo/client'

export const ArchiveGameMutation = gql`
    mutation ArchiveGame($gameId: SID!) {
        archiveGame(gameId: $gameId) {
            id
        }
    }
`
