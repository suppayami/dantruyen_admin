import { gql } from '@apollo/client'

export const UpdateScoreMutation = gql`
    mutation UpdateScore($gameId: SID!, $score: Int!) {
        adminUpdateScore(gameId: $gameId, score: $score) {
            gameId
            score
        }
    }
`
