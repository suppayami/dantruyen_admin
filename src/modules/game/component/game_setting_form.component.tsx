import * as React from 'react'
import { useQuery, useMutation } from '@apollo/client'
import { useForm, OnSubmit } from 'react-hook-form'
import { object, number } from 'yup'

import { Input } from '@/common/components/input.component'
import { Game, GameVariables } from '@/modules/game/api/gql-types/Game'
import { GameQuery } from '@/modules/game/api/game.graphql'
import { Button } from '@/common/components/button.component'
import PostEditor from '@/modules/editor/component/editor.component'
import {
    getProseMirrorData,
    versioningProseMirror,
} from '@/modules/editor/utils/prose_mirror.utils'
import { GameMode, GameUpdateInput, AdultType } from '@/common/global.type'
import { UpdateGameVariables, UpdateGame } from '@/modules/game/api/gql-types/UpdateGame'
import { UpdateGameMutation } from '@/modules/game/api/update_game.graphql'
import { notifyInfo, notifyError } from '@/common/notification/toast'
import { Config } from '@/common/config'
import { directUpload } from '@/common/service/upload'

interface GameSettingFormProps {
    gid: string
}

const UpdateGameSchema = object().shape<GameUpdateInput>({
    actLengthFirst: number().positive().integer(),
    actLengthSecond: number().positive().integer(),
    actLengthThird: number().positive().integer(),
    playerLimit: number().positive().integer(),
    wordLimitNarrator: number().positive().integer(),
    wordLimitPlayer: number().positive().integer(),
})

export const GameSettingForm: React.FunctionComponent<GameSettingFormProps> = ({ gid }) => {
    const [thumbnail, setThumbnail] = React.useState<File | undefined>(undefined)
    const [cover, setCover] = React.useState<File | undefined>(undefined)

    const { data } = useQuery<Game, GameVariables>(GameQuery, {
        skip: !gid,
        variables: { gameId: gid },
        fetchPolicy: 'cache-only',
    })
    const [updateGame] = useMutation<UpdateGame, UpdateGameVariables>(UpdateGameMutation)
    const {
        register,
        handleSubmit,
        setValue,
        watch,
        formState: { isSubmitting },
    } = useForm<GameUpdateInput>({
        mode: 'onSubmit',
        reValidateMode: 'onBlur',
        validationSchema: UpdateGameSchema,
        defaultValues: {
            name: data?.game?.name,
            thumbnailUrl: data?.game?.thumbnailUrl,
            coverImageUrl: data?.game?.coverImageUrl,
            description: getProseMirrorData(data?.game?.description ?? ''),
            gameMode: data?.game?.gameDetail.gameMode,
            storyLength: data?.game?.gameDetail.storyLength,
            actLengthFirst: data?.game?.gameDetail.actLengthFirst,
            actLengthSecond: data?.game?.gameDetail.actLengthSecond,
            actLengthThird: data?.game?.gameDetail.actLengthSecond,
            gameSpeed: data?.game?.gameDetail.gameSpeed,
            wordLimitMode: data?.game?.gameDetail.wordLimitMode,
            wordLimitPlayer: data?.game?.gameDetail.wordLimitPlayer,
            wordLimitNarrator: data?.game?.gameDetail.wordLimitNarrator,
            playerLimit: data?.game?.gameDetail.playerLimit,
            licenseMode: data?.game?.gameDetail.licenseMode,
            ccLicense: data?.game?.gameDetail.ccLicense,
            additionalRules: getProseMirrorData(data?.game?.gameDetail.additionalRules ?? ''),
            contact: data?.game?.gameDetail.contact,
            adult: data?.game?.gameDetail.adult,
            visibility: data?.game?.gameDetail.visibility,
        },
    })
    const onSubmit: OnSubmit<GameUpdateInput> = React.useCallback(
        async (data) => {
            if (isSubmitting) {
                notifyInfo('Từ từ làm gì nóng thế')
                return
            }
            notifyInfo('Đang update game, chờ tí')
            try {
                let coverImageUrl = data.coverImageUrl
                let thumbnailUrl = data.thumbnailUrl

                if (cover) {
                    coverImageUrl = (await directUpload(cover, 'cover')).filePath
                }

                if (thumbnail) {
                    thumbnailUrl = (await directUpload(thumbnail, 'thumbnail')).filePath
                }

                await updateGame({
                    variables: {
                        gameId: gid,
                        gameUpdateInput: Object.assign({}, data, {
                            description: JSON.stringify(data.description),
                            additionalRules: JSON.stringify(data.additionalRules),
                            thumbnailUrl,
                            coverImageUrl,
                        }),
                    },
                    refetchQueries: [{ query: GameQuery, variables: { gameId: gid } }],
                })
                notifyInfo('Update thành công')
            } catch (error) {
                notifyError(error.message)
            }
        },
        [gid, updateGame, isSubmitting, cover, thumbnail],
    )
    const watchStoryLength = watch('storyLength')
    const watchWordLimitMode = watch('wordLimitMode')
    const watchLicenseMode = watch('licenseMode')

    React.useEffect(() => {
        register({ name: 'description' })
        register({ name: 'additionalRules' })
        register({ name: 'thumbnailUrl' })
        register({ name: 'coverImageUrl' })
    }, [register])

    if (!data?.game) {
        return <div />
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Input type="text" name="name" label="Tên phòng chơi" ref={register} />
            <div>
                <span>Miêu tả</span>
                <div className="border">
                    <PostEditor
                        premiumState={data.game.premiumState}
                        data={getProseMirrorData(data.game.description ?? '')}
                        onChange={(proseMirror) =>
                            setValue(
                                'description',
                                versioningProseMirror(proseMirror.state.toJSON().doc),
                            )
                        }
                    />
                </div>
            </div>
            <label className="block w-64 my-2">
                Game Mode
                <select className="block w-full px-4 py-2 border" name="gameMode" ref={register}>
                    <option value={GameMode.BALANCE}>Balance</option>
                    <option value={GameMode.GAME_FOCUS}>Game Focus</option>
                    <option value={GameMode.WRITE_FOCUS}>Write Focus</option>
                </select>
            </label>
            <label className="block w-64 my-2">
                Độ dài
                <select className="block w-full px-4 py-2 border" name="storyLength" ref={register}>
                    <option value="short">Truyện ngắn</option>
                    <option value="novella">Truyện vừa</option>
                    <option value="novel">Truyện dài</option>
                    <option value="custom">Tùy chọn</option>
                </select>
                {watchStoryLength === 'custom' && (
                    <div className="w-16">
                        <Input type="number" name="actLengthFirst" label="Phần 1" ref={register} />
                        <Input type="number" name="actLengthSecond" label="Phần 2" ref={register} />
                        <Input type="number" name="actLengthThird" label="Phần 3" ref={register} />
                    </div>
                )}
            </label>
            <label className="block w-64 my-2">
                Tốc độ chơi
                <select className="block w-full px-4 py-2 border" name="gameSpeed" ref={register}>
                    <option value="slowest">Thoải mái</option>
                    <option value="slow">Chậm</option>
                    <option value="normal">Bình thường</option>
                    <option value="fast">Đều đặn</option>
                    <option value="fastest">Nhanh</option>
                </select>
            </label>
            <label className="block w-64 my-2">
                Giới hạn từ
                <select
                    className="block w-full px-4 py-2 border"
                    name="wordLimitMode"
                    ref={register}
                >
                    <option value="default">Mặc định</option>
                    <option value="unlimited">Không giới hạn</option>
                    <option value="custom">Tùy chọn</option>
                </select>
                {watchWordLimitMode === 'custom' && (
                    <div className="w-32">
                        <Input
                            type="number"
                            name="wordLimitPlayer"
                            label="Người chơi"
                            ref={register}
                        />
                        <Input
                            type="number"
                            name="wordLimitNarrator"
                            label="Người dẫn truyện"
                            ref={register}
                        />
                    </div>
                )}
            </label>
            <Input
                className="w-16"
                type="number"
                name="playerLimit"
                label="Giới hạn người chơi"
                ref={register}
            />
            <label className="block w-64 my-2">
                Quyền sở hữu
                <select className="block w-full px-4 py-2 border" name="licenseMode" ref={register}>
                    <option value="shared">Chung</option>
                    <option value="sharedCC">Chung + Creative Commons</option>
                    <option value="host">Người chủ trì</option>
                </select>
                {watchLicenseMode === 'sharedCC' && (
                    <label className="block w-64 my-2">
                        Creative Commons
                        <select
                            className="block w-full px-4 py-2 border"
                            name="ccLicense"
                            ref={register}
                        >
                            <option value="CC BY 4.0">Attribution</option>
                            <option value="CC BY-SA 4.0">Attribution-ShareAlike </option>
                            <option value="CC BY-ND 4.0">Attribution-NoDerivs</option>
                            <option value="CC BY-NC 4.0">Attribution-NonCommercial</option>
                            <option value="CC BY-NC-SA 4.0">
                                Attribution-NonCommercial-ShareAlike
                            </option>
                            <option value="CC BY-NC-ND 4.0">
                                Attribution-NonCommercial-NoDerivs
                            </option>
                        </select>
                    </label>
                )}
            </label>
            <div>
                <span>Nội quy</span>
                <div className="border">
                    <PostEditor
                        premiumState={data.game.premiumState}
                        data={getProseMirrorData(data.game.gameDetail.additionalRules ?? '')}
                        onChange={(proseMirror) =>
                            setValue(
                                'additionalRules',
                                versioningProseMirror(proseMirror.state.toJSON().doc),
                            )
                        }
                    />
                </div>
            </div>
            <Input type="text" name="contact" label="Thông tin liên lạc" ref={register} />
            <label className="block w-64 my-2">
                Chế độ người nhớn
                <select className="block w-full px-4 py-2 border" name="adult" ref={register}>
                    <option value={AdultType.ADULT}>Người nhớn</option>
                    <option value={AdultType.EVERYONE}>Vô tư</option>
                </select>
            </label>
            <label className="block w-64 my-2">
                Hiển thị
                <select className="block w-full px-4 py-2 border" name="visibility" ref={register}>
                    <option value="public">Công cộng</option>
                    <option value="close">Riêng tư</option>
                </select>
            </label>

            <div>
                Thumbnail
                <img
                    src={
                        thumbnail
                            ? URL.createObjectURL(thumbnail)
                            : `${Config.userCDN}${data.game.thumbnailUrl}`
                    }
                    alt="thumbnail"
                />
                <Input type="file" onChange={(event) => setThumbnail(event.target.files?.[0])} />
            </div>

            <div>
                Cover
                <img
                    src={
                        cover
                            ? URL.createObjectURL(cover)
                            : `${Config.userCDN}${data.game.coverImageUrl}`
                    }
                    alt="cover"
                />
                <Input type="file" onChange={(event) => setCover(event.target.files?.[0])} />
            </div>

            <Button className="mt-2">Save</Button>
        </form>
    )
}
