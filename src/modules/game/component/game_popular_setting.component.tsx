import * as React from 'react'
import { useQuery, useMutation } from '@apollo/client'

import { Input } from '@/common/components/input.component'
import { Game, GameVariables } from '@/modules/game/api/gql-types/Game'
import { GameQuery } from '@/modules/game/api/game.graphql'
import { UpdateScore, UpdateScoreVariables } from '@/modules/game/api/gql-types/UpdateScore'
import { UpdateScoreMutation } from '@/modules/game/api/update_score.graphql'
import { notifyInfo } from '@/common/notification/toast'
import { Button } from '@/common/components/button.component'

interface GamePopularSettingProps {
    gid: string
}

export const GamePopularSetting: React.FunctionComponent<GamePopularSettingProps> = ({ gid }) => {
    const { data } = useQuery<Game, GameVariables>(GameQuery, {
        skip: !gid,
        variables: { gameId: gid },
        fetchPolicy: 'cache-only',
        onCompleted(data) {
            setScore(data.game?.popularScore?.score ?? 0)
        },
    })
    const [updateScore] = useMutation<UpdateScore, UpdateScoreVariables>(UpdateScoreMutation)
    const [score, setScore] = React.useState(0)

    if (!data?.game) {
        return <div />
    }

    return (
        <form
            onSubmit={async (event) => {
                event.preventDefault()
                notifyInfo('Chờ tẹo update score nhoa')
                await updateScore({ variables: { gameId: gid, score } })
                notifyInfo('Update score xong rồi nha hjhj')
            }}
        >
            <h3>Với cái này thì phải bấm nút submit bên dưới nhoa</h3>
            <Input
                type="number"
                label="Popular Score"
                value={score}
                onChange={(event) => setScore(parseInt(event.target.value) ?? 0)}
            />
            <Button variant="primary">Submit Score</Button>
        </form>
    )
}
