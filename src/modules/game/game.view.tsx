import * as React from 'react'
import { useQuery, useMutation } from '@apollo/client'
import { useRouter } from 'next/router'

import { Game, GameVariables } from '@/modules/game/api/gql-types/Game'
import { GameQuery } from '@/modules/game/api/game.graphql'
import { ContentSection } from '@/common/components/content_section.component'
import { GameSettingForm } from '@/modules/game/component/game_setting_form.component'
import { Button } from '@/common/components/button.component'
import { ArchiveGame, ArchiveGameVariables } from '@/modules/game/api/gql-types/ArchiveGame'
import { ArchiveGameMutation } from '@/modules/game/api/archive_game.graphql'
import { Input } from '@/common/components/input.component'
import { notifyInfo } from '@/common/notification/toast'
import { GamePopularSetting } from '@/modules/game/component/game_popular_setting.component'

interface GameViewProps {
    gid: string
}

export const GameView: React.FunctionComponent<GameViewProps> = ({ gid }) => {
    const { data, loading, error } = useQuery<Game, GameVariables>(GameQuery, {
        skip: !gid,
        variables: { gameId: gid },
        fetchPolicy: 'cache-and-network',
    })
    const [archiveGame] = useMutation<ArchiveGame, ArchiveGameVariables>(ArchiveGameMutation)
    const [confirmDelete, setConfirmDelete] = React.useState('')
    const router = useRouter()

    if (loading) {
        return <ContentSection>Loading...</ContentSection>
    }

    if (error) {
        return <ContentSection>{error.message}</ContentSection>
    }

    return (
        <ContentSection>
            <h3 className="py-4 text-xl font-bold">Sửa phòng chơi: {data?.game?.name}</h3>
            <GameSettingForm gid={gid} />
            <Button
                variant="danger"
                className="my-2"
                disabled={confirmDelete !== 'ok'}
                onClick={async () => {
                    notifyInfo('Đang xóa, chờ tí hjhj')
                    await archiveGame({ variables: { gameId: gid } })
                    notifyInfo('Xóa xong rồi, quay về nhoa')
                    router.replace('/games')
                }}
            >
                Xóa phòng
            </Button>
            <Input
                type="text"
                label="Điền ok vào đây để xác nhận xóa phòng"
                value={confirmDelete}
                onChange={(event) => setConfirmDelete(event.target.value)}
            />

            <div className="mt-16" />

            <GamePopularSetting gid={gid} />
        </ContentSection>
    )
}
