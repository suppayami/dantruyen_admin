import { gql } from '@apollo/client'

export const GiveTemBatchMutation = gql`
    mutation GiveTemBatch($amount: Int!, $emails: [String!]!) {
        adminGiveTemBatch(amount: $amount, userEmails: $emails) {
            id
            orderDescription
            orderId
            payAmount
            payState
            unit
        }
    }
`
