import { gql } from '@apollo/client'

export const GiveTemMutation = gql`
    mutation GiveTem($userId: SID!, $amount: Int!) {
        adminGiveTem(amount: $amount, userId: $userId) {
            id
            orderDescription
            orderId
            payAmount
            payState
            unit
        }
    }
`
