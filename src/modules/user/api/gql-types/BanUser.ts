/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: BanUser
// ====================================================

export interface BanUser_adminBanUser {
  __typename: "User";
  id: any;
}

export interface BanUser {
  adminBanUser: BanUser_adminBanUser;
}

export interface BanUserVariables {
  userId: any;
}
