/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PaymentState, CurrencyUnit } from "./../../../../common/global.type";

// ====================================================
// GraphQL mutation operation: GiveTemBatch
// ====================================================

export interface GiveTemBatch_adminGiveTemBatch {
  __typename: "CurrencyTransaction";
  id: any;
  orderDescription: string;
  orderId: string;
  payAmount: number;
  payState: PaymentState;
  unit: CurrencyUnit;
}

export interface GiveTemBatch {
  adminGiveTemBatch: (GiveTemBatch_adminGiveTemBatch | null)[];
}

export interface GiveTemBatchVariables {
  amount: number;
  emails: string[];
}
