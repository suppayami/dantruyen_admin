/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PaymentState, PaymentUnit, CurrencyUnit } from "./../../../../common/global.type";

// ====================================================
// GraphQL query operation: TransactionList
// ====================================================

export interface TransactionList_adminTransactionList {
  __typename: "Transaction";
  id: any;
  orderDescription: string;
  orderId: string;
  payAmount: number;
  payState: PaymentState;
  unit: PaymentUnit;
}

export interface TransactionList_adminCurrencyTransactionList {
  __typename: "CurrencyTransaction";
  id: any;
  orderDescription: string;
  orderId: string;
  payAmount: number;
  payState: PaymentState;
  unit: CurrencyUnit;
}

export interface TransactionList {
  adminTransactionList: TransactionList_adminTransactionList[];
  adminCurrencyTransactionList: TransactionList_adminCurrencyTransactionList[];
}

export interface TransactionListVariables {
  userId: any;
}
