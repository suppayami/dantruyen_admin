/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PaymentState, CurrencyUnit } from "./../../../../common/global.type";

// ====================================================
// GraphQL mutation operation: GiveTem
// ====================================================

export interface GiveTem_adminGiveTem {
  __typename: "CurrencyTransaction";
  id: any;
  orderDescription: string;
  orderId: string;
  payAmount: number;
  payState: PaymentState;
  unit: CurrencyUnit;
}

export interface GiveTem {
  adminGiveTem: GiveTem_adminGiveTem;
}

export interface GiveTemVariables {
  userId: any;
  amount: number;
}
