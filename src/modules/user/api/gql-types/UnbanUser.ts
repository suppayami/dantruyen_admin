/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UnbanUser
// ====================================================

export interface UnbanUser_adminUnbanUser {
  __typename: "User";
  id: any;
}

export interface UnbanUser {
  adminUnbanUser: UnbanUser_adminUnbanUser;
}

export interface UnbanUserVariables {
  userId: any;
}
