import { gql } from '@apollo/client'

export const UnbanUserMutation = gql`
    mutation UnbanUser($userId: SID!) {
        adminUnbanUser(userId: $userId) {
            id
        }
    }
`
