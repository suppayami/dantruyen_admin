import { gql } from '@apollo/client'

export const BanUserMutation = gql`
    mutation BanUser($userId: SID!) {
        adminBanUser(userId: $userId) {
            id
        }
    }
`
