import { gql } from '@apollo/client'

export const TransactionListQuery = gql`
    query TransactionList($userId: SID!) {
        adminTransactionList(userId: $userId) {
            id
            orderDescription
            orderId
            payAmount
            payState
            unit
        }
        adminCurrencyTransactionList(userId: $userId) {
            id
            orderDescription
            orderId
            payAmount
            payState
            unit
        }
    }
`
