import * as React from 'react'

import { TransactionList } from '@/modules/user/api/gql-types/TransactionList'

interface TransactionTableProps {
    data: TransactionList
}

export const TransactionTable: React.FunctionComponent<TransactionTableProps> = ({ data }) => {
    return (
        <table className="table-auto">
            <thead>
                <tr>
                    <th className="px-4 py-2">ID</th>
                    <th className="px-4 py-2">Description</th>
                    <th className="px-4 py-2">Amount</th>
                    <th className="px-4 py-2">State</th>
                    <th className="px-4 py-2">Unit</th>
                </tr>
            </thead>
            <tbody>
                {data.adminTransactionList.map((transaction) => (
                    <tr key={transaction.id}>
                        <td className="px-4 py-2 border">{transaction.orderId}</td>
                        <td className="px-4 py-2 border">{transaction.orderDescription}</td>
                        <td className="px-4 py-2 border">{transaction.payAmount}</td>
                        <td className="px-4 py-2 border">{transaction.payState}</td>
                        <td className="px-4 py-2 border">{transaction.unit}</td>
                    </tr>
                ))}
            </tbody>
            <tbody>
                {data.adminCurrencyTransactionList.map((transaction) => (
                    <tr key={transaction.id}>
                        <td className="px-4 py-2 border">{transaction.orderId}</td>
                        <td className="px-4 py-2 border">{transaction.orderDescription}</td>
                        <td className="px-4 py-2 border">{transaction.payAmount}</td>
                        <td className="px-4 py-2 border">{transaction.payState}</td>
                        <td className="px-4 py-2 border">{transaction.unit}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    )
}
