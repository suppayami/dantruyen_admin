import { gql } from '@apollo/client'

export const CreateEditorChoicesMutation = gql`
    mutation CreateEditorChoices($gameIds: [SID!]!) {
        createEditorChoices(gameIds: $gameIds) {
            id
        }
    }
`
