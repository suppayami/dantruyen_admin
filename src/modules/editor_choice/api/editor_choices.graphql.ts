import { gql } from '@apollo/client'

export const EditorChoicesQuery = gql`
    query EditorChoices {
        editorChoices {
            id
            game {
                id
                name
                slug
            }
        }
    }
`
