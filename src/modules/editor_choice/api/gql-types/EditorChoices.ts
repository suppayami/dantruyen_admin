/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: EditorChoices
// ====================================================

export interface EditorChoices_editorChoices_game {
  __typename: "Game";
  id: any;
  name: string;
  slug: string;
}

export interface EditorChoices_editorChoices {
  __typename: "EditorChoice";
  id: any;
  game: EditorChoices_editorChoices_game;
}

export interface EditorChoices {
  editorChoices: EditorChoices_editorChoices[];
}
