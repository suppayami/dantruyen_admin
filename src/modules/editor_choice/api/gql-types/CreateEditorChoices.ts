/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateEditorChoices
// ====================================================

export interface CreateEditorChoices_createEditorChoices {
  __typename: "EditorChoice";
  id: any;
}

export interface CreateEditorChoices {
  createEditorChoices: CreateEditorChoices_createEditorChoices[];
}

export interface CreateEditorChoicesVariables {
  gameIds: any[];
}
