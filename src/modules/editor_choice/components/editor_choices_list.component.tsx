import * as React from 'react'
import { useQuery } from '@apollo/client'

import { EditorChoicesQuery } from '@/modules/editor_choice/api/editor_choices.graphql'
import { ContentSection } from '@/common/components/content_section.component'
import { EditorChoices } from '@/modules/editor_choice/api/gql-types/EditorChoices'

export const EditorChoicesList: React.FunctionComponent = () => {
    const { data, loading, error } = useQuery<EditorChoices>(EditorChoicesQuery, {
        fetchPolicy: 'cache-and-network',
    })

    if (loading) {
        return <ContentSection>Loading...</ContentSection>
    }

    if (error) {
        return <ContentSection>{error.message}</ContentSection>
    }

    return (
        <ContentSection>
            <ul>
                {data?.editorChoices.map((editorChoice) => (
                    <li key={editorChoice.id}>
                        {editorChoice.game.name} - {editorChoice.game.id}
                    </li>
                ))}
            </ul>
        </ContentSection>
    )
}
