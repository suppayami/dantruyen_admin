import { useState, useCallback, useMemo } from 'react'
import { EditorState } from 'prosemirror-state'
import { EditorView } from 'prosemirror-view'
import { Schema, Node } from 'prosemirror-model'
import { schema, marks } from 'prosemirror-schema-basic'
import { addListNodes } from 'prosemirror-schema-list'
import { exampleSetup, buildMenuItems } from 'prosemirror-example-setup'
import { MenuItem, blockTypeItem } from 'prosemirror-menu'

import { versioningProseMirror } from '@/modules/editor/utils/prose_mirror.utils'
import { PremiumState } from '@/common/global.type'

interface ProseMirrorOptions {
    data?: any
    premiumState?: PremiumState
}

function canInsert(state: any, nodeType: any) {
    const $from = state.selection.$from
    for (let d = $from.depth; d >= 0; d--) {
        const index = $from.index(d)
        if ($from.node(d).canReplaceWith(index, index, nodeType)) {
            return true
        }
    }
    return false
}

export const proseMirrorBlank = versioningProseMirror({
    type: 'doc',
    content: [{ type: 'paragraph', content: [] }],
})
export const proseMirrorCorrupted = (data: string) => ({
    type: 'doc',
    content: data.split('\n').map((line) => ({
        type: 'paragraph',
        content: line ? [{ type: 'text', text: line }] : undefined,
    })),
})

export const useProseMirror = <T extends HTMLElement>(
    options?: ProseMirrorOptions,
): [EditorView | null, (node: T) => void] => {
    const isPremium = options?.premiumState === PremiumState.PREMIUM
    const [view, setView] = useState<EditorView | null>(null)
    const editorSchema = useMemo(() => {
        let nodes = (schema.spec.nodes as any).remove('image').remove('code_block')

        if (!isPremium) {
            nodes = nodes.remove('blockquote').remove('horizontal_rule').remove('heading')
        }

        return new Schema({
            nodes: isPremium ? addListNodes(nodes, 'paragraph block*', 'block') : nodes,
            marks: isPremium ? Object.assign({}, marks, {}) : ({} as any),
        })
    }, [isPremium])

    const menuBar = useMemo(() => {
        if (!isPremium) {
            return { fullMenu: [[]] }
        }
        const menu = buildMenuItems(editorSchema)
        menu.fullMenu = menu.inlineMenu.concat([
            [
                menu.wrapBulletList,
                menu.wrapOrderedList,
                menu.wrapBlockQuote,
                new MenuItem({
                    title: 'Insert horizontal rule',
                    label: 'HR',
                    css: 'cursor: pointer;',
                    enable(state: any) {
                        return canInsert(state, editorSchema.nodes.horizontal_rule)
                    },
                    run(state: any, dispatch: any) {
                        dispatch(
                            state.tr.replaceSelectionWith(
                                editorSchema.nodes.horizontal_rule.create(),
                            ),
                        )
                    },
                } as any),
                blockTypeItem(editorSchema.nodes.heading, {
                    title: 'Change to heading 1',
                    label: 'H1',
                    attrs: { level: 1 },
                }),
                blockTypeItem(editorSchema.nodes.heading, {
                    title: 'Change to heading 2',
                    label: 'H2',
                    attrs: { level: 2 },
                }),
                blockTypeItem(editorSchema.nodes.heading, {
                    title: 'Change to heading 3',
                    label: 'H3',
                    attrs: { level: 3 },
                }),
                blockTypeItem(editorSchema.nodes.heading, {
                    title: 'Change to heading 4',
                    label: 'H4',
                    attrs: { level: 4 },
                }),
                blockTypeItem(editorSchema.nodes.heading, {
                    title: 'Change to heading 5',
                    label: 'H5',
                    attrs: { level: 5 },
                }),
                blockTypeItem(editorSchema.nodes.heading, {
                    title: 'Change to heading 6',
                    label: 'H6',
                    attrs: { level: 6 },
                }),
            ],
        ])
        return menu
    }, [editorSchema, isPremium])

    const ref = useCallback(
        (node: T | null) => {
            if (node !== null) {
                let doc: any

                try {
                    doc = Node.fromJSON(editorSchema, options?.data || proseMirrorBlank)
                } catch {
                    doc = Node.fromJSON(editorSchema, proseMirrorCorrupted(`${options?.data}`))
                }

                setView(
                    new EditorView(node, {
                        state: EditorState.create({
                            doc,
                            plugins: exampleSetup({
                                schema: editorSchema,
                                menuContent: menuBar.fullMenu,
                                menuBar: isPremium,
                            }),
                        }),
                    }),
                )
            }
        },
        [editorSchema, menuBar, isPremium],
    )

    return [view, ref]
}
