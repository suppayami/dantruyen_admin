import isString from 'lodash/isString'

const VERSION = 'v1'

export const countWords = (data: any) => {
    let count = 0

    if (!data.content) {
        return 0
    }

    for (const content of data.content) {
        if (content.type === 'text') {
            count += content.text.trim().split(' ').filter(Boolean).length
            continue
        }
        count += countWords(content)
    }

    return count
}

export const versioningProseMirror = (data: any) => Object.assign({}, data, { dtVersion: VERSION })
export const getProseMirrorData = (data: any) => {
    try {
        const result = JSON.parse(data)
        if (result.dtVersion && result.dtVersion === VERSION) {
            return result
        }
        return `${data}`
    } catch {
        return `${data}`
    }
}

export const proseMirrorToPlainText = (data: any) => {
    const parsedData = getProseMirrorData(data)

    if (isString(parsedData)) {
        return parsedData
    }

    return parsedData.content.map(contentMap).join('\n')
}

const contentMap = (content: any) => {
    const childrenContent = content.content ?? []

    if (!content.type) {
        return JSON.stringify(content)
    }

    if (content.type === 'paragraph') {
        if (childrenContent.length === 0) {
            return '\n'
        }

        return childrenContent.map(contentMap).join('\n')
    }

    if (content.type === 'text') {
        return content.text ?? ''
    }

    try {
        return childrenContent.map(contentMap).join('\n')
    } catch {
        return JSON.stringify(content)
    }
}
