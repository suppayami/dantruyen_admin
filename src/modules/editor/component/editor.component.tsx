import React, { useEffect } from 'react'
import { EditorView } from 'prosemirror-view'

import { PremiumState } from '@/common/global.type'
import { useProseMirror } from '@/modules/editor/hook/prose_mirror.hook'

interface PostEditorProps {
    onChange?: (proseMirror: EditorView) => void
    data?: any
    premiumState?: PremiumState
}

export const PostEditor: React.FunctionComponent<PostEditorProps> = ({
    onChange,
    data,
    premiumState,
}) => {
    const [proseMirror, proseMirrorRef] = useProseMirror<HTMLDivElement>({ data, premiumState })

    useEffect(() => {
        if (proseMirror) {
            if (onChange) {
                onChange(proseMirror)
            }
            proseMirror.props.dispatchTransaction = (transaction) => {
                const newState = proseMirror.state.apply(transaction)
                proseMirror.updateState(newState)
                if (onChange) {
                    onChange(proseMirror)
                }
            }
        }
    }, [proseMirror, onChange])

    return (
        <div className="dt-new">
            <div ref={proseMirrorRef} className="post-editor-pm post-content" />
        </div>
    )
}

// eslint-disable-next-line import/no-default-export
export default PostEditor
