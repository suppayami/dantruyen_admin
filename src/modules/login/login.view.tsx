import * as React from 'react'
import { useForm, OnSubmit } from 'react-hook-form'
import { object, string } from 'yup'
import { useRouter } from 'next/router'

import { Input } from '@/common/components/input.component'
import { FormErrorMessage } from '@/common/components/form_error_message.component'
import { useSessionStore } from '@/modules/session/hook/session_store.hook'
import { isAuthenticated } from '@/modules/session/store/authenticate.state'

type Inputs = {
    username: string
    password: string
}

const LoginSchema = object().shape<Inputs>({
    username: string().required(),
    password: string().required(),
})

export const LoginView: React.FunctionComponent = () => {
    const { register, handleSubmit, errors } = useForm<Inputs>({
        validationSchema: LoginSchema,
        mode: 'onSubmit',
        reValidateMode: 'onBlur',
    })
    const router = useRouter()
    const sessionStore = useSessionStore()
    const onSubmit: OnSubmit<Inputs> = React.useCallback(
        async (data) => {
            await sessionStore.authenticate(data)
            if (isAuthenticated(sessionStore.authenticateState)) {
                router.push('/')
            }
        },
        [router, sessionStore],
    )

    return (
        <div className="max-w-full w-96">
            <form onSubmit={handleSubmit(onSubmit)}>
                <h1 className="text-2xl">Login</h1>

                {errors.username && <FormErrorMessage>Username is required</FormErrorMessage>}
                {errors.password && <FormErrorMessage>Password is required</FormErrorMessage>}

                <Input id="username" name="username" type="text" label="Username" ref={register} />
                <Input
                    id="password"
                    name="password"
                    type="password"
                    label="Password"
                    ref={register}
                />

                <div>
                    <button type="submit" className="px-4 py-2 text-white bg-blue-700">
                        Login
                    </button>
                </div>
            </form>
        </div>
    )
}
