import * as React from 'react'

export const SingleBlockLayout: React.FunctionComponent = ({ children }) => {
    return (
        <div className="flex items-center h-screen bg-gray-200">
            <div className="flex items-center max-w-md p-8 mx-auto bg-white shadow-xl">
                {children}
            </div>
        </div>
    )
}
