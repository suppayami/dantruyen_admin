import * as React from 'react'

import { Navigation } from '@/layouts/main_layout/navigation.component'

export const MainLayout: React.FunctionComponent = ({ children }) => {
    return (
        <div className="flex h-screen">
            <div className="flex w-64">
                <Navigation />
            </div>
            <div className="flex-1 overflow-y-auto">
                <div className="p-4">{children}</div>
            </div>
        </div>
    )
}
