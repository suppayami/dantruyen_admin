import * as React from 'react'
import Link from 'next/link'
import { observer } from 'mobx-react-lite'

import { useSessionStore } from '@/modules/session/hook/session_store.hook'
import { ButtonLink } from '@/common/components/button_link.component'
import { Anchor } from '@/common/components/anchor.component'
import { isAuthenticated } from '@/modules/session/store/authenticate.state'

export const Navigation: React.FunctionComponent = observer(function Navigation() {
    const sessionStore = useSessionStore()

    if (!isAuthenticated(sessionStore.authenticateState)) {
        return (
            <section className="w-full pt-4 pl-6 text-white bg-gray-900">
                <h1 className="py-4 text-xl">Dan Truyen Admin</h1>
                <h3>Loading...</h3>
            </section>
        )
    }

    return (
        <section className="w-full pt-4 pl-6 text-white bg-gray-900">
            <h1 className="py-4 text-xl">Dan Truyen Admin</h1>

            <ul className="py-4">
                <li className="py-1">{sessionStore.authenticateState.profile.displayName}</li>
                <li className="py-1">
                    <ButtonLink variant="light" onClick={sessionStore.logout}>
                        Logout
                    </ButtonLink>
                </li>
            </ul>

            <ul className="py-4">
                <li className="py-1">
                    <Link href="/" as="/">
                        <Anchor href="/" variant="light">
                            Home
                        </Anchor>
                    </Link>
                </li>
                <li className="py-1">
                    <Link href="/games" as="/games">
                        <Anchor href="/games" variant="light">
                            Games
                        </Anchor>
                    </Link>
                </li>
                <li className="py-1">
                    <Link href="/users" as="/users">
                        <Anchor href="/users" variant="light">
                            Users
                        </Anchor>
                    </Link>
                </li>
                <li className="py-1">
                    <Link href="/editor-choices" as="/editor-choices">
                        <Anchor href="/editor-choices" variant="light">
                            Editor's Choices
                        </Anchor>
                    </Link>
                </li>
            </ul>
        </section>
    )
})
