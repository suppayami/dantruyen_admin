import { ApolloClient, NormalizedCacheObject } from '@apollo/client'
import { NextPageContext } from 'next'
import { toJS } from 'mobx'

import { getApolloClient } from '@/common/service/apollo'
import { Config } from '@/common/config'
import { Store, getStore } from '@/common/store/store'

import { isBrowser } from './is_platform.ssr'

type Managers = {
    apollo: ApolloClient<NormalizedCacheObject>
    store: Store
}
export type StateConstructor = (managers: Managers) => Promise<void>

export const getInitialState = (context: NextPageContext) => (
    constructor: StateConstructor,
) => async <T>(props: Promise<T> | T) => {
    if (isBrowser()) {
        return props
    }

    const apollo = getApolloClient({
        apiEndpoint: `${Config.siteUrl}${Config.apiEndpoint}`,
        cookie: context.req?.headers.cookie,
        ssr: true,
    })
    const store = getStore()

    try {
        await constructor({ apollo, store })
    } catch (error) {
        // Catch error so that server won't crash
        console.error(error)
        // TODO: Notify to bugs tracker
    }

    return {
        ...(await props),
        apolloState: apollo.cache.extract(),
        initialState: toJS(store),
    }
}
