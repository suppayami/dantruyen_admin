import i18next from 'i18next'
import { initReactI18next } from 'react-i18next'

import VICommon from './vi/common.json'
import VINotification from './vi/notification.json'

export const i18n = i18next.createInstance().use(initReactI18next)

const initI18n = () =>
    i18n.init({
        lng: 'vi',
        fallbackLng: 'vi',
        debug: false,

        ns: ['common', 'notification'],
        defaultNS: 'common',

        resources: {
            vi: {
                common: VICommon,
                notification: VINotification,
            },
        },
    })

initI18n()
