import * as React from 'react'

export const ContentSection: React.FunctionComponent = ({ children }) => (
    <section>{children}</section>
)
