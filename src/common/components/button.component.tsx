import * as React from 'react'

import { classnames } from '@/common/utils/classnames'

type ButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
    variant?: 'primary' | 'danger'
}

export const Button = React.forwardRef<HTMLButtonElement, ButtonProps>(
    ({ variant = 'primary', className, children, disabled, ...props }, ref) => (
        <button
            className={classnames(
                'border',
                'px-4',
                'py-2',
                variant === 'primary' && ['text-white', 'bg-blue-600'],
                variant === 'danger' && ['text-white', 'bg-red-600'],
                disabled && ['opacity-25', 'cursor-not-allowed'],
                className,
            )}
            ref={ref}
            {...props}
        >
            {children}
        </button>
    ),
)
