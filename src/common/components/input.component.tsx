import * as React from 'react'

import { classnames } from '@/common/utils/classnames'

export type InputProps = React.InputHTMLAttributes<HTMLInputElement> & {
    label?: string
}

export const Input = React.forwardRef<HTMLInputElement, InputProps>(
    ({ label, className, ...props }, ref) => {
        return (
            <label className="flex flex-col my-2">
                {label ?? ''}
                <input ref={ref} className={classnames('border', 'p-2', className)} {...props} />
            </label>
        )
    },
)
