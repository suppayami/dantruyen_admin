import * as React from 'react'

import { classnames } from '@/common/utils/classnames'

export type TextareaProps = React.TextareaHTMLAttributes<HTMLTextAreaElement> & {
    label?: string
}

export const Textarea = React.forwardRef<HTMLTextAreaElement, TextareaProps>(
    ({ label, className, ...props }, ref) => {
        return (
            <label className="flex flex-col my-2">
                {label ?? ''}
                <textarea ref={ref} className={classnames('border', 'p-2', className)} {...props} />
            </label>
        )
    },
)
