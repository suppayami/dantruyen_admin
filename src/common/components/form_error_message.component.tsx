import * as React from 'react'

export const FormErrorMessage: React.FunctionComponent = ({ children }) => (
    <div className="text-red-700">{children}</div>
)
