import * as React from 'react'

import { classnames } from '@/common/utils/classnames'

type AnchorProps = React.AnchorHTMLAttributes<HTMLAnchorElement> & {
    variant?: 'light' | 'dark'
}

export const Anchor = React.forwardRef<HTMLAnchorElement, AnchorProps>(
    ({ variant = 'dark', className, children, ...props }, ref) => (
        <a
            className={classnames(
                variant === 'light' && 'text-blue-200',
                variant === 'dark' && 'text-blue-700',
                'underline',
                className,
            )}
            ref={ref}
            {...props}
        >
            {children}
        </a>
    ),
)
