import * as React from 'react'

import { classnames } from '@/common/utils/classnames'

type ButtonLinkProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
    variant?: 'light' | 'dark'
}

export const ButtonLink = React.forwardRef<HTMLButtonElement, ButtonLinkProps>(
    ({ variant = 'dark', className, children, ...props }, ref) => (
        <button
            className={classnames(
                variant === 'light' && 'text-blue-200',
                variant === 'dark' && 'text-blue-700',
                'underline',
                className,
            )}
            ref={ref}
            {...props}
        >
            {children}
        </button>
    ),
)
