import * as React from 'react'
import { autorun } from 'mobx'

export const useAutorun = (fn: () => void) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    React.useEffect(() => autorun(fn), [])
}
