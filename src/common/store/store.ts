import * as React from 'react'
import { configure } from 'mobx'

import 'mobx-react-lite/batchingForReactDom'

import { makeSessionStore, SessionStore } from '@/modules/session/store/session.store'
import { isBrowser } from '@/ssr/is_platform.ssr'

configure({ enforceActions: 'always' })

export interface Store {
    sessionStore: SessionStore
}

let currentStore: Store

const makeStore = (initialState?: Partial<Store>): Store => ({
    sessionStore: makeSessionStore(initialState?.sessionStore),
})

export const getStore = (initialState?: Partial<Store>): Store => {
    if (!isBrowser()) {
        return makeStore(initialState)
    }
    if (!currentStore) {
        currentStore = makeStore(initialState)
    }
    return currentStore
}

export const StoreContext = React.createContext<Store>(getStore())
StoreContext.displayName = 'StoreContext'
