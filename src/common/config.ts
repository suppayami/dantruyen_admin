export const Config = {
    siteName: 'Dan Truyen - AdminCP',
    apiEndpoint: '/graphql',
    userCDN: 'https://d2jcbbv6tauwnr.cloudfront.net',
    siteUrl:
        process.env.NODE_ENV === 'development'
            ? 'http://localhost:3000'
            : 'https://admin.dantruyen.com',
}
