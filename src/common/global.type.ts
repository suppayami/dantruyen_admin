/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * Adult Type
 */
export enum AdultType {
  ADULT = "ADULT",
  EVERYONE = "EVERYONE",
}

export enum CurrencyUnit {
  TEM = "TEM",
}

export enum GameMode {
  BALANCE = "BALANCE",
  GAME_FOCUS = "GAME_FOCUS",
  WRITE_FOCUS = "WRITE_FOCUS",
}

export enum PaymentState {
  PAID = "PAID",
  PENDING = "PENDING",
}

export enum PaymentUnit {
  VND = "VND",
}

/**
 * Premium State Type
 */
export enum PremiumState {
  NORMAL = "NORMAL",
  PREMIUM = "PREMIUM",
}

export interface GameUpdateInput {
  actLengthFirst?: number | null;
  actLengthSecond?: number | null;
  actLengthThird?: number | null;
  additionalRules?: string | null;
  adult?: AdultType | null;
  ccLicense?: string | null;
  characterCreation?: string | null;
  contact?: string | null;
  coverImageUrl?: string | null;
  description?: string | null;
  gameMode?: GameMode | null;
  gameSpeed?: string | null;
  licenseMode?: string | null;
  name?: string | null;
  playerLimit?: number | null;
  storyGuideMode?: string | null;
  storyLength?: string | null;
  thumbnailUrl?: string | null;
  visibility?: string | null;
  wordLimitMode?: string | null;
  wordLimitNarrator?: number | null;
  wordLimitPlayer?: number | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
