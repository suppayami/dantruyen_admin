import { GraphQLError } from 'graphql'
import { ApolloError } from '@apollo/client'

export const isGraphqlError = (error: any): error is ApolloError =>
    error instanceof ApolloError || error.graphQLErrors
export const getErrorExtensions = (error: ApolloError) =>
    error.graphQLErrors.reduce((prev, cur) => ({ ...prev, ...(cur.extensions ?? {}) }), {})

export const getUnauthorized = (error: ApolloError) =>
    error.graphQLErrors.find(
        (gqlError) => gqlError.extensions && gqlError.extensions.http_code === 401,
    )

export const getValidationError = (error: ApolloError) =>
    error.graphQLErrors.find((gqlError) => gqlError.message === 'validation_error')

export const getGameLogicError = (error: ApolloError) =>
    error.graphQLErrors.find((gqlError) => gqlError.message === 'game_logic_error')

export const containsBannedWordError = (error: GraphQLError) =>
    error.extensions?.message === 'contains banned words'

export const isLimitedDraftGame = (error: ApolloError) =>
    error.graphQLErrors.some((gqlError) => gqlError.extensions?.code === 'limit_draft_game')
