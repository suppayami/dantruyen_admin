import { onError } from '@apollo/link-error'
import { GraphQLError } from 'graphql'
import { Observable } from '@apollo/client'

import { getStore } from '@/common/store/store'

const needRenewSession = (errors: readonly GraphQLError[]) =>
    errors.some((gqlError) => gqlError.extensions && gqlError.extensions.http_code === 401)

const RenewSessionOperationName = 'RenewSession'

const renewSession = async (): Promise<string> => {
    const store = getStore()
    await store.sessionStore.renewSession(true)
    return ''
}

export const makeRenewSessionLink = () =>
    onError(({ networkError, graphQLErrors, operation, forward }) => {
        if (networkError) {
            return forward(operation)
        }

        if (graphQLErrors && needRenewSession(graphQLErrors)) {
            if (operation.operationName === RenewSessionOperationName) {
                return
            }

            return new Observable<string>((subscriber) => {
                renewSession().then(
                    (result) => {
                        if (subscriber.closed) {
                            return
                        }
                        subscriber.next(result)
                        subscriber.complete()
                    },
                    (err) => {
                        subscriber.next(err)
                        subscriber.complete()
                        // return forward(operation)
                    },
                )
            }).flatMap((result) => {
                const headers = operation.getContext().headers
                operation.setContext({
                    ...headers,
                    Authorization: result ? `Bearer ${result}` : '',
                })
                return forward(operation)
            })
        }

        return
    })
