import fetch from 'isomorphic-unfetch'

import { Config } from '@/common/config'
import { getStore } from '@/common/store/store'
import { isAuthenticated } from '@/modules/session/store/authenticate.state'

export const directUpload = async (
    file: File,
    type: 'cover' | 'thumbnail' | 'character' = 'cover',
): Promise<{ filePath: string }> => {
    const data = new FormData()
    data.append('file', file)
    data.append('img_type', type)

    const store = getStore()

    await store.sessionStore.renewSession(true)

    if (!isAuthenticated(store.sessionStore.authenticateState)) {
        throw new Error('Unauthorized')
    }

    const result = await fetch(`${Config.apiEndpoint}/upload`, {
        method: 'POST',
        body: data,
        credentials: 'same-origin',
        headers: {
            authorization: `Bearer ${store.sessionStore.authenticateState.token}`,
        },
    })

    if (!result.ok) {
        const json = await result.json()
        throw new Error(json)
    }

    return result.json()
}
