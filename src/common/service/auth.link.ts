import { setContext } from '@apollo/link-context'

export const getToken = () => sessionStorage.getItem('_dantruyen_admin_token')
export const setToken = (token: string) => sessionStorage.setItem('_dantruyen_admin_token', token)

export const setRemember = (flag = true) =>
    localStorage.setItem('_dantruyen_admin_logged_in', `${flag}`)
export const isRemember = () => localStorage.getItem('_dantruyen_admin_logged_in') === 'true'

export const makeAuthLink = (initToken?: string) =>
    setContext((_, { headers }) => {
        const token = initToken ?? getToken()

        return {
            headers: {
                ...headers,
                Authorization: token ? `Bearer ${token}` : '',
            },
        }
    })
