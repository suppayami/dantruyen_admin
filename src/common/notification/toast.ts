import { toast } from 'react-toastify'

export const notifyError = toast.error
export const notifyInfo = toast.info
export const notifySuccess = toast.success
