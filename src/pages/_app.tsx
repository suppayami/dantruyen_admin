import '@/common/tailwind.scss'

import * as React from 'react'
import { AppProps } from 'next/app'
import { ApolloProvider } from '@apollo/client'
import { I18nextProvider } from 'react-i18next'
import Head from 'next/head'
import { ToastContainer, Slide } from 'react-toastify'

import { getApolloClient } from '@/common/service/apollo'
import { i18n } from '@/common/i18n/i18n'
import { LayoutPage } from '@/types'
import { Config } from '@/common/config'
import { StoreContext, getStore } from '@/common/store/store'
import { isServer } from '@/ssr/is_platform.ssr'
import { Protected } from '@/modules/session/component/protected.component'

// eslint-disable-next-line import/no-default-export
export default function App({ Component, pageProps }: AppProps) {
    const { initialState, apolloState } = pageProps

    const apolloClient = React.useMemo(() => {
        return getApolloClient({
            apiEndpoint: `${Config.apiEndpoint}`,
            initialState: apolloState,
            ssr: isServer(),
        })
    }, [])
    const store = React.useMemo(() => {
        return getStore(initialState)
    }, [])
    const LayoutedComponent = Component as LayoutPage
    const Layout = LayoutedComponent.layout ?? React.Fragment

    return (
        <I18nextProvider i18n={i18n}>
            <ApolloProvider client={apolloClient}>
                <StoreContext.Provider value={store}>
                    <Head>
                        <title>{Config.siteName}</title>
                    </Head>
                    <div className="font-sans">
                        <Layout>
                            <Protected isProtected={LayoutedComponent.isProtected}>
                                <Component {...pageProps} />
                                <ToastContainer
                                    position="bottom-right"
                                    hideProgressBar={true}
                                    newestOnTop={true}
                                    draggable={false}
                                    transition={Slide}
                                    autoClose={10000}
                                />
                            </Protected>
                        </Layout>
                    </div>
                </StoreContext.Provider>
            </ApolloProvider>
        </I18nextProvider>
    )
}
