import * as React from 'react'

import { LayoutPage } from '@/types'
import { LoginView } from '@/modules/login/login.view'
import { SingleBlockLayout } from '@/layouts/single_block.layout'

export const LoginPage: LayoutPage = () => {
    return <LoginView />
}

// eslint-disable-next-line import/no-default-export
export default LoginPage

LoginPage.layout = SingleBlockLayout
