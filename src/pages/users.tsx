import * as React from 'react'
import { useMutation, useLazyQuery } from '@apollo/client'
import chunk from 'lodash/chunk'

import { LayoutPage } from '@/types'
import { MainLayout } from '@/layouts/main_layout/main.layout'
import { ContentSection } from '@/common/components/content_section.component'
import { Input } from '@/common/components/input.component'
import { Button } from '@/common/components/button.component'
import { BanUser, BanUserVariables } from '@/modules/user/api/gql-types/BanUser'
import { BanUserMutation } from '@/modules/user/api/ban_user.graphql'
import { notifyInfo, notifySuccess } from '@/common/notification/toast'
import { UnbanUserMutation } from '@/modules/user/api/unban_user.graphql'
import { UnbanUser, UnbanUserVariables } from '@/modules/user/api/gql-types/UnbanUser'
import {
    TransactionListVariables,
    TransactionList,
} from '@/modules/user/api/gql-types/TransactionList'
import { TransactionListQuery } from '@/modules/user/api/transaction_list.graphql'
import { TransactionTable } from '@/modules/user/component/transaction_table.component'
import { GiveTem, GiveTemVariables } from '@/modules/user/api/gql-types/GiveTem'
import { GiveTemMutation } from '@/modules/user/api/give_tem.graphql'
import { GiveTemBatchMutation } from '@/modules/user/api/give_tem_batch.graphql'
import { GiveTemBatch, GiveTemBatchVariables } from '@/modules/user/api/gql-types/GiveTemBatch'
import { Textarea } from '@/common/components/textarea.component'

export const UsersPage: LayoutPage = () => {
    const [userId, setUserId] = React.useState('')
    const [tem, setTem] = React.useState(0)
    const [emails, setEmails] = React.useState('')
    const [query, { data, loading, error }] = useLazyQuery<
        TransactionList,
        TransactionListVariables
    >(TransactionListQuery)
    const [banUser] = useMutation<BanUser, BanUserVariables>(BanUserMutation)
    const [unbanUser] = useMutation<UnbanUser, UnbanUserVariables>(UnbanUserMutation)
    const [giveTem] = useMutation<GiveTem, GiveTemVariables>(GiveTemMutation)
    const [giveTemBatch] = useMutation<GiveTemBatch, GiveTemBatchVariables>(GiveTemBatchMutation)

    const banUserHandler: React.MouseEventHandler = async (event) => {
        event.preventDefault()
        notifyInfo('Chờ tẹo để ban')
        await banUser({ variables: { userId } })
        notifyInfo('Ban xong rồi hjhj')
    }

    const unbanUserHandler: React.MouseEventHandler = async (event) => {
        event.preventDefault()
        notifyInfo('Chờ tẹo để unban')
        await unbanUser({ variables: { userId } })
        notifyInfo('Unban xong rồi hjhj')
    }

    const fetchTransactionHandler: React.MouseEventHandler = async (event) => {
        event.preventDefault()
        query({ variables: { userId } })
    }

    const giveTemHandler: React.MouseEventHandler = async (event) => {
        event.preventDefault()
        notifyInfo('Chờ tẹo để tặng tèm tem')
        await giveTem({
            variables: { userId, amount: tem },
            refetchQueries: [{ query: TransactionListQuery, variables: { userId } }],
        })
        notifyInfo('Tèm tem thành công')
    }

    const giveTemBatchHandler: React.MouseEventHandler = async (event) => {
        event.preventDefault()
        notifyInfo('Chờ tẹo để tặng tèm tem')
        const split = emails.split(/[\n\r]+/).filter(Boolean)
        const emailList = chunk(split, 100)
        const total = split.length
        let done = 0

        for (let i = 0; i < emailList.length; i++) {
            const { data } = await giveTemBatch({
                variables: { emails: emailList[i], amount: tem },
            })
            done += data?.adminGiveTemBatch?.filter(Boolean).length ?? 0
            notifyInfo(`Tèm tem progress: ${done}/${total}`)
        }

        notifySuccess(`Tèm tem thành công, done: ${done}/${total}`)
    }

    return (
        <ContentSection>
            <h3 className="py-4 text-xl font-bold">Users</h3>
            <p className="my-4">Điền user ID xuống đây rồi làm trò con bò nhoa.</p>

            <form className="max-w-lg mt-8">
                <Input
                    type="text"
                    id="user-id"
                    name="user-id"
                    label="User ID"
                    value={userId}
                    onChange={(event) => setUserId(event.target.value)}
                />
                <Input
                    type="number"
                    id="tem-amoumt"
                    name="tem-amount"
                    label="Tặng tem"
                    value={tem}
                    onChange={(event) => setTem(parseInt(event.target.value || '0', 10))}
                />
                <Textarea
                    label="Email List (Mỗi email 1 dòng)"
                    rows={8}
                    onChange={(event) => setEmails(event.target.value)}
                />
                <Button onClick={banUserHandler}>Ban User</Button>
                <Button onClick={unbanUserHandler}>Unban User</Button>
                <Button onClick={fetchTransactionHandler}>Fetch Transactions</Button>
                <Button onClick={giveTemHandler}>Tặng tem</Button>
                <Button onClick={giveTemBatchHandler}>Tặng tem theo emails</Button>
            </form>

            <section className="my-8">
                {loading && 'Loading...'}
                {error && JSON.stringify(error)}
                {data && <TransactionTable data={data} />}
            </section>
        </ContentSection>
    )
}

// eslint-disable-next-line import/no-default-export
export default UsersPage

UsersPage.layout = MainLayout
UsersPage.isProtected = true
