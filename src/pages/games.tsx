import * as React from 'react'
import { useRouter } from 'next/router'
import { useMutation } from '@apollo/client'

import { LayoutPage } from '@/types'
import { MainLayout } from '@/layouts/main_layout/main.layout'
import { Input } from '@/common/components/input.component'
import { Button } from '@/common/components/button.component'
import { ContentSection } from '@/common/components/content_section.component'
import { MakePremium, MakePremiumVariables } from '@/modules/game/api/gql-types/MakePremium'
import { MakePremiumMutation } from '@/modules/game/api/make_premium.graphql'
import { notifyInfo, notifySuccess, notifyError } from '@/common/notification/toast'

export const GamesPage: LayoutPage = () => {
    const [gid, setGID] = React.useState('')
    const router = useRouter()
    const [makePremium] = useMutation<MakePremium, MakePremiumVariables>(MakePremiumMutation)

    const makePremiumHandler: React.MouseEventHandler = async (event) => {
        event.preventDefault()

        notifyInfo('Chờ tí là lên premium nhoa hihi')
        try {
            await makePremium({ variables: { gameId: gid } })
            notifySuccess('Lên rồi nhoa hihi')
        } catch (error) {
            notifyError(error.message)
        }
    }

    return (
        <ContentSection>
            <h3 className="py-4 text-xl font-bold">Games</h3>
            <p className="my-4">Điền game ID vào ô dưới đây rồi bấm súp mít nhoa.</p>
            <small>
                Tìm ID trong URL của phòng: https://www.dantruyen.com/game/tu-an-
                <strong>DaUDfKPhvjf0</strong>
            </small>

            <form
                className="max-w-lg mt-8"
                onSubmit={(event) => {
                    event.preventDefault()
                    router.push({ pathname: '/games/[gid]', query: { gid } }, `/games/${gid}`)
                }}
            >
                <Input
                    type="text"
                    id="game-id"
                    name="game-id"
                    label="Game ID"
                    onChange={(event) => setGID(event.target.value)}
                />
                <Button>Submit</Button>
                <Button onClick={makePremiumHandler}>Make Premium</Button>
            </form>
        </ContentSection>
    )
}

// eslint-disable-next-line import/no-default-export
export default GamesPage

GamesPage.layout = MainLayout
GamesPage.isProtected = true
