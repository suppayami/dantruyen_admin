import * as React from 'react'
import { useRouter } from 'next/router'

import { LayoutPage } from '@/types'
import { MainLayout } from '@/layouts/main_layout/main.layout'
import { GameView } from '@/modules/game/game.view'

export const GameSpecificPage: LayoutPage = () => {
    const router = useRouter()
    const gid = router?.query?.gid as string

    return <GameView gid={gid} />
}

// eslint-disable-next-line import/no-default-export
export default GameSpecificPage

GameSpecificPage.layout = MainLayout
GameSpecificPage.isProtected = true
