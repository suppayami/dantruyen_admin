import * as React from 'react'

import { MainLayout } from '@/layouts/main_layout/main.layout'
import { LayoutPage } from '@/types'
import { ContentSection } from '@/common/components/content_section.component'

export const HomePage: LayoutPage = () => {
    return (
        <ContentSection>
            <h3 className="py-4 text-xl font-bold">Đây là cái homepage, cứ từ từ rồi tính tiếp.</h3>
            <p className="my-4">Nói chung thì chả biết cho gì vào đây, cứ nghĩ đi nhé hjhj.</p>
        </ContentSection>
    )
}

// eslint-disable-next-line import/no-default-export
export default HomePage

HomePage.layout = MainLayout
HomePage.isProtected = true
