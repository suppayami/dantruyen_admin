import * as React from 'react'
import { useMutation } from '@apollo/client'

import { LayoutPage } from '@/types'
import { MainLayout } from '@/layouts/main_layout/main.layout'
import { Button } from '@/common/components/button.component'
import { ContentSection } from '@/common/components/content_section.component'
import { Textarea } from '@/common/components/textarea.component'
import { EditorChoicesList } from '@/modules/editor_choice/components/editor_choices_list.component'
import { CreateEditorChoicesMutation } from '@/modules/editor_choice/api/create_editor_choices.graphql'
import {
    CreateEditorChoices,
    CreateEditorChoicesVariables,
} from '@/modules/editor_choice/api/gql-types/CreateEditorChoices'
import { notifyInfo, notifySuccess } from '@/common/notification/toast'
import { EditorChoicesQuery } from '@/modules/editor_choice/api/editor_choices.graphql'

export const EditorChoicesPage: LayoutPage = () => {
    const [gids, setGids] = React.useState('')
    const [createEditorChoices] = useMutation<CreateEditorChoices, CreateEditorChoicesVariables>(
        CreateEditorChoicesMutation,
    )
    const submitHandler: React.FormEventHandler = async (event) => {
        event.preventDefault()

        const gameIds = gids.split(/[\n\r]+/).filter(Boolean)
        notifyInfo(`Waiting for creating editor's choices`)
        await createEditorChoices({
            variables: { gameIds },
            refetchQueries: [{ query: EditorChoicesQuery }],
        })
        notifySuccess(`Successfully Created editor's choices`)
    }

    return (
        <ContentSection>
            <h3 className="py-4 text-xl font-bold">Editor's Choices</h3>
            <p className="my-4">Điền Game IDs vào ô dưới này, mỗi ID 1 dòng nhé hjhj.</p>
            <small>
                Tìm ID trong URL của phòng: https://www.dantruyen.com/game/tu-an-
                <strong>DaUDfKPhvjf0</strong>
            </small>

            <div className="my-4">
                <h3 className="font-bold">Editor's Choices hiện tại:</h3>
                <EditorChoicesList />
            </div>

            <form className="max-w-lg mt-8" onSubmit={submitHandler}>
                <Textarea
                    label="Game IDs (Mỗi ID 1 dòng)"
                    rows={8}
                    onChange={(event) => setGids(event.target.value)}
                />
                <Button>Submit</Button>
            </form>
        </ContentSection>
    )
}

// eslint-disable-next-line import/no-default-export
export default EditorChoicesPage

EditorChoicesPage.layout = MainLayout
EditorChoicesPage.isProtected = true
